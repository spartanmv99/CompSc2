close all
clear all
clc

%% Generation of 100 samples of WN(3,4) = 3+2*WN(0,1) --> 3 realizations
figure; hold on
for i = 1:3
    e = 3+2*randn(1,100);

    plot(e,'-*')
end
xlabel('Samples')