# SETS

set I;
set J;

# PARAMS

param c{I};
param b{I};
param r{J};
param q_max{I,J} default 1;
param q_min{I,J} default 0;

# VAR

var y{J} >= 0, integer;
var x{I,J} >= 0, integer; 

# OBJECTIVE FUNCTION

maximize revenue:
	sum{i in I, j in J} x[i, j]*r[j] - sum{i in I, j in J} c[i] * x[i, j];
	
# CONSTRAINTS

subject to Aviability{i in I}:
	sum{j in J} x[i, j] <= b[i];
	
subject to RequirementsMax{j in J, i in I}:
	x[i, j] <= q_max[i, j]*y[j];
	
subject to RequirementsMin{j in J, i in I}:
	x[i, j] >= q_min[i, j]*y[j];
	
subject to conservation{j in J}:
	y[j] = sum{i in I} x[i,j];