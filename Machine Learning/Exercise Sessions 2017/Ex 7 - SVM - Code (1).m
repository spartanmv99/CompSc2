clear
clc
close all

addpath(genpath('.'));
%% Initialization
load iris_dataset;

irisInputs = zscore(irisInputs(1:2,1:100)');
%irisInputs = irisInputs(1:2,1:100)';
%irisInputs = [50 * irisInputs(1,1:100)' zscore(irisInputs(2,1:100))'];

irisTargets = [ones(50,1); -ones(50,1)];

figure();
gplotmatrix(irisInputs,[],irisTargets);

svm_model = fitcsvm(irisInputs,irisTargets,'Verbose',1,'NumPrint',1);
%svm_model = fitcsvm(irisInputs,irisTargets,'Verbose',1,'NumPrint',1,'BoxConstraint',200);

w = svm_model.Beta;
b = svm_model.Bias;

%% Visualize the linear SVM
figure();
pos_class = irisTargets == 1;
neg_class = irisTargets == -1;
plot(irisInputs(pos_class,1), irisInputs(pos_class,2),'r.');
hold on;
plot(irisInputs(neg_class,1), irisInputs(neg_class,2),'g.');

% Classes bound
x = min(irisInputs(:,1)):0.1:max(irisInputs(:,1));
y = -w(1) / w(2) * x - b / w(2);
plot(x,y);

%Margins
y = -w(1) / w(2) * x + (1 - b) / w(2);
plot(x,y,'--');
y = -w(1) / w(2) * x + (-1 - b) / w(2);
plot(x,y,'--');
xlabel('x_1');
ylabel('x_2');
axis tight

M = 1 / norm(w)

%%
% Check correctly classified points
ind = irisTargets .* (b + irisInputs * w) > 0;
sum(ind)

% Support vectors
support_vec = svm_model.SupportVectors;
plot(support_vec(:,1),support_vec(:,2),'rx');

%%
% Use a Gaussian kernel
C = 1;
bandwidth = 0.5;

svm_kernel_model = fitcsvm(irisInputs, irisTargets, 'KernelFunction', 'gaussian', 'boxconstraint', C, 'KernelScale', bandwidth);

% Visualize the Kernel SVM
min_x = min(irisInputs);
max_x = max(irisInputs);
n_p = 50;
x = linspace(min_x(1),max_x(1),n_p);
y = linspace(min_x(2),max_x(2),n_p);
[X,Y] = meshgrid(x,y);
[~,z] = predict(svm_kernel_model,[X(:) Y(:)]);
Z = reshape(z(:,1),n_p,n_p) / 50;

figure();
pos_class = irisTargets == 1;
neg_class = irisTargets == -1;
plot(irisInputs(pos_class,1), irisInputs(pos_class,2),'r.');
hold on;
plot(irisInputs(neg_class,1), irisInputs(neg_class,2),'g.');
contour(X,Y,Z);
xlabel('x_1');
ylabel('x_2');
title(['C = ' num2str(C) ', l = ' num2str(bandwidth)]);
axis tight

support_vec = svm_kernel_model.SupportVectors;
plot(support_vec(:,1), support_vec(:,2),'rx');

%% Solve the dual form with SMO
manual_svm = svm_smo(irisInputs,irisTargets,1,0.0001,4)

w = manual_svm.w;
figure();
plot(irisInputs(pos_class,1), irisInputs(pos_class,2),'r.');
hold on;
plot(irisInputs(neg_class,1), irisInputs(neg_class,2),'g.');

% Classes bound
x = min(irisInputs(:,1)):0.1:max(irisInputs(:,1));
y = -w(1) / w(2) * x - b / w(2);
plot(x,y);

%Margins
y = -w(1) / w(2) * x + (1 - b) / w(2);
plot(x,y,'--');
y = -w(1) / w(2) * x + (-1 - b) / w(2);
plot(x,y,'--');
xlabel('x_1');
ylabel('x_2');
axis tight
